package com.rahasak.kerapoththa.repo

import com.rahasak.kerapoththa.config.StorageConf
import com.rahasak.kerapoththa.model._
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object KerapoththaRepo extends AccountsTable with BalancesTable with TransactionsTable with BlocksTable with StorageConf {

  /**
   * Create database tables with slick
   */
  def init(): Future[List[Unit]] = {
    val f1 = db.run(transactions.schema.createIfNotExists)
    val f2 = db.run(balances.schema.createIfNotExists)
    val f3 = db.run(accounts.schema.createIfNotExists)
    val f4 = db.run(blocks.schema.createIfNotExists)

    Future.sequence(List(f1, f2, f3, f4))
  }

  /**
   * Create transaction
   */
  def createTransaction(transaction: Transaction): Future[Int] = {
    db.run(transactions += transaction)
  }

  /**
   * Get transaction with given `id` and execer` fields
   */
  def getTransaction(id: String, execer: String): Future[Option[Transaction]] = {
    db.run(transactions.filter(t => t.id === id && t.execer === execer).result.headOption)
  }

  /**
   * Create block
   * Block contains list of transactions as postgres array type field
   * To handle array types in Slick `KerapoththaPostgresProfile` implemented the `PgArraySupport` function
   */
  def createBlock(block: Block): Future[Int] = {
    db.run(blocks += block)
  }

  /**
   * Get block with given id
   */
  def getBlock(id: String): Future[Option[Block]] = {
    db.run(blocks.filter(_.id === id).result.headOption)
  }

  /**
   * Create account
   */
  def createAccount(account: Account): Future[Int] = {
    db.run(accounts += account)
  }

  /**
   * Get account with given address
   */
  def getAccount(address: String): Future[Option[Account]] = {
    db.run(accounts.filter(_.address === address).result.headOption)
  }

  /**
   * Create balance of account
   */
  def createBalance(balance: Balance): Future[Int] = {
    db.run(balances += balance)
  }

  /**
   * Get balance of account
   */
  def getBalance(account: String): Future[Option[Balance]] = {
    db.run(balances.filter(_.account === account).result.headOption)
  }

  /**
   * Transfer fund from `fromAddress` to `toAddress`
   * On a transfer, balance will be credited to `fromAddress` and debited from `toAddress`
   * Two queries used to handle credit/debit functions and executed as Slick transaction
   * Currently Slick does not support to use + or - in queries. So I have used plain sql queries to facilitate
   * these functions
   */
  def transfer(fromAddress: String, toAddress: String, amount: Long): Future[Unit] = {
    val t1 =
      sqlu"""
          update balances
          set value = value + $amount
          where account = $toAddress
          """
    val t2 =
      sqlu"""
          update balances
          set value = value - $amount
          where account = $fromAddress
          """

    val combinedAction = DBIO.seq(t1, t2)
    db.run(combinedAction.transactionally)
  }

}

