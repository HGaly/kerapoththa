package com.rahasak.kerapoththa.model

import com.github.tminglei.slickpg._
import com.rahasak.kerapoththa.config.StorageConf

case class Block(id: String, transactions: List[String], merkleRoot: String, preHash: String, hash: String, digsig: String, timestamp: Long)

trait KerapoththaPostgresProfile extends ExPostgresProfile with PgArraySupport {
  override protected def computeCapabilities: Set[slick.basic.Capability] =
    super.computeCapabilities + slick.jdbc.JdbcCapabilities.insertOrUpdate

  override val api = KerapoththaPostgresAPI

  object KerapoththaPostgresAPI extends API with ArrayImplicits {
    implicit val strListTypeMapper = new SimpleArrayJdbcType[String]("text").to(_.toList)
  }
}

object KerapoththaPostgresProfile extends KerapoththaPostgresProfile

import KerapoththaPostgresProfile.api._

trait BlocksTable {
  this: StorageConf =>

  class Blocks(tag: Tag) extends Table[Block](tag, "blocks") {
    def id = column[String]("id", O.PrimaryKey, O.Unique)

    def transactions = column[List[String]]("transactions", O.Default(Nil))

    def merkleRoot = column[String]("merkle_root")

    def preHash = column[String]("pre_hash")

    def hash = column[String]("hash")

    def digsig = column[String]("digsig")

    def timestamp = column[Long]("timestamp")

    // select
    def * = (id, transactions, merkleRoot, preHash, hash, digsig, timestamp) <> (Block.tupled, Block.unapply)
  }

  val blocks = TableQuery[Blocks]
}

